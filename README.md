# Extended 3
Show the weather, time, and more on your secondary monitor when you aren't using it

To change the Apple TV Screensavers that are used, download your favorite day and night screensavers from [here](http://benjaminmayo.co.uk/watch-all-the-apple-tv-aerial-video-screensavers) and replace day.mov and night.mov in resources/app with the videos you downloaded. You can also use your own day and night videos if they are in the mov format.

To show your SoundCloud status in Extended, install the [SoundCloud Connection for Extended](https://chrome.google.com/webstore/detail/soundcloud-connection-for/bkfhjidmicgldnjofheokbpabmpbcpch?hl=en) extension on Google Chrome. You must be using SoundCloud in Google Chrome for the extension to work.

To exit Extended, click the clock at the right bottom corner of the screen.
